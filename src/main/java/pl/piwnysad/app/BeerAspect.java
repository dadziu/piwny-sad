package pl.piwnysad.app;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class BeerAspect {

    @Pointcut("execution(* pl.piwnysad.app.BeerController.oneBeer(..))")
    public void oneBeerPointcut(){};

    @Before("oneBeerPointcut()")
    public void availability(){
        System.out.println("Czy jest w lodowce?");
    }

    @After("oneBeerPointcut()")
    public void delivery(){
        System.out.println("Dostarczono klientowi");
    }
}
