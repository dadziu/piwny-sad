package pl.piwnysad.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiwnySadApplication {

    public static void main(String[] args) {
        SpringApplication.run(PiwnySadApplication.class, args);
    }
}
