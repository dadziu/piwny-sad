package pl.piwnysad.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class Config {

    @Bean
    BeerController beerController(){
        return new BeerController();
    }

    @Bean
    BeerAspect beerAspect() {
        return new BeerAspect();
    }
}
