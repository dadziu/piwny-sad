package pl.piwnysad.app;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import pl.piwnysad.implementation.Beer;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequestMapping("beers")
//@Controller //standard mechanism of Spring MVC to create model(for example .jsp site)
@RestController //identify created model as REST service
public class BeerController {

    /**
     * Method that contains and returns list of all beers
     *
     * @return list of beers
     */
    @RequestMapping("allBeers")
    public /*@ResponseBody*/ List<Beer> getBeers() { //@RB saying to make this result as REST

        List<Beer> beers = new ArrayList<>();

        beers.add(new Beer("Warka", 4.5 , 3.20));
        beers.add(new Beer("Tyskie", 5.2, 4.10));
        beers.add(new Beer("Lech", 5.5, 3.99));
        beers.add(new Beer("Zywiec", 5.8, 3.70));
        beers.add(new Beer("Ciechan", 6.2, 5.30));

        return beers;
    }

    /**
     * Method that returns name of beer inputted into url address
     *
     * @param name
     * @return name of beer
     */
    @GetMapping("getOne/{oneBeer}")
    public String oneBeer(@PathVariable("oneBeer") String name) {
        return "Zamowione piwko: " + name;
    }

    /**
     * Method that returns Beer with parameters from url address
     *
     * @param name
     * @param alcoholPercentage
     * @param price
     * @return Beer object with parameters from url address
     */
    @PostMapping("add")
    public Beer addBeer(
        @RequestParam String name, //default is required
        @RequestParam(required = false) Double alcoholPercentage, //(required = false) - can't be primitive type
        @RequestParam double price
        //http://localhost:8080/beers/add?name=warka&alcoholPercentage=3.4&price=7.5
    ){
        return new Beer(name, alcoholPercentage, price);
    }
}